#!/usr/bin/env python3
import socket
import sys
from PIL import Image
from time import sleep


sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(('192.168.4.1', 8000))

img = Image.open(sys.argv[1])
img = img.convert('RGB')

for y in range(img.size[1]):
    pixel = []
    for x in range(img.size[0]):
        r, g, b = img.getpixel((x, y))
        pixel += [r, g, b]
    line = bytes([y + 128] + pixel)
    sock.send(line)
    if y >= 127:
        break
