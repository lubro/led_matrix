#include "PxMatrix.h"
#include <WiFi.h>


const char* ssid = "ESP23a";
const char* password =  "123456789";


PxMATRIX * my_matrix = NULL;
WiFiServer server(8000);

void setup() {
  Serial.begin(9600);
  //Serial.begin(115200);

  Serial.println("Start");
  WiFi.softAP(ssid, password);
  /*while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
   Serial.println("Connected to the WiFi network");
   Serial.println(WiFi.localIP());
  // Define your display layout here, e.g. 1/8 step
  */
  server.begin();

  my_matrix = setup_matrix();
}


void draw_pixeltest(){
  uint16_t j = 0;
  for(uint16_t i=0; i<64; i++){
    my_matrix->drawPixel(i,j,32000);
    j++;
  }
   j = 63;
  for(uint16_t i=0; i<64; i++){
    my_matrix->drawPixel(i,j,13370);
    j--;
  }
}

void draw_more(uint16_t x, uint16_t y){
  uint16_t i = 0;
  my_matrix->drawPixel(x,y+3,32000);
  my_matrix->drawPixel(x,y+4,32000);
  x ++;
  my_matrix->drawPixel(x,y+5,32000);
  x++;
  my_matrix->drawPixel(x,y+6,32000);
  x++;
  my_matrix->drawPixel(x,y,32000);
  my_matrix->drawPixel(x,y+1,32000);
  my_matrix->drawPixel(x,y+6,32000);

  for(i=0; i<5; i++){
      x++;
  my_matrix->drawPixel(x,y+6,32000);
  }
  x ++;
  my_matrix->drawPixel(x,y,32000);
  my_matrix->drawPixel(x,y+1,32000);
  my_matrix->drawPixel(x,y+6,32000);
  x++;
  my_matrix->drawPixel(x,y+6,32000);
  x++;
  my_matrix->drawPixel(x,y+5,32000);
  x++;
  my_matrix->drawPixel(x,y+4,32000);
  my_matrix->drawPixel(x,y+3,32000);
}

uint8_t get_num(char * input){
	uint8_t output;
	output += (input[0] -48) * 10;
	output += (input[1] -48);
	return output;
}

uint16_t get_color(char * input){
	uint8_t r, g, b;
	r =(input[0] -48) * 100;
	r +=(input[1] -48) * 10;
	r +=(input[2] -48);

	g =(input[3] -48) * 100;
	g +=(input[4] -48) * 10;
	g +=(input[5] -48);

	b =(input[6] -48) * 100;
	b +=(input[7] -48) * 10;
	b +=(input[8] -48);
  
  Serial.printf("R %d, G %d B %d \n", r, g, b);
  
	return my_matrix->color565(r,g,b);
}

void readline(WiFiClient * client, char * buffer, uint8_t maximum){
	char c;
	for(uint8_t i = 0; i < maximum; i++){
		c = client->read();
		if(c==10){
			buffer[i] = 0;
			break;
		}
		buffer[i] = c;
	}
}

void loop(){
 // display.clearDisplay();
   uint8_t c, color;
   uint8_t input[192];
   uint8_t y;

   char input_line[10]; 
   WiFiClient client = server.available(); // listen for incoming clients
   if (client) {
    Serial.print("new Client");
    String currentLine = ""; 
    while(client.connected()){
      if (client.available()) {             // if there's bytes to read from the client,
        //readline(&client, input_line, 10);
        c = client.read();
        if(c >= 128){
            c = c-128; // Save Row in c
            client.read(input, 192); //Read Row
            y = 0;
            for(size_t i = 0; i < 192; i+=3){
                //Draw Pixel
                my_matrix->drawPixel(y, c, my_matrix->color565(*(input+i), *(input+i+1), *(input+i+2)));
                y ++;
            }
        }else if(c == 0){
            my_matrix->clearDisplay();
        }else{
          break;
        }
  /*
  if (*input_line == 's'){
      x = get_num(input_line + 1);
      y = get_num(input_line + 3);
      draw_more(x, y);
  }else if(*input_line == 'p'){
      x = get_num(input_line + 1);
      y = get_num(input_line + 3);
      color = get_color(input_line + 5);

      //my_matrix->drawPixel(x, y, color);
      my_matrix->drawPixel(x, y, my_matrix->color565(200, 100, 0));

  }else if(*input_line == 'c'){
    my_matrix->clearDisplay();
  }
  */
      }
    }
   }
  //draw_pixeltest();
  //delay(300);
}
