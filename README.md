LED-Matrix 64x64 Network Display
================================

In this repository you can find an implementation to transfer pictures
over the network to 64x64 led matrix.

Examples are based on a 64x64 ESP32 LED-Matrix implementation.

Matrix-Api:
---------------------------------------------------------------------
The LED-Matrix APi is a simplified version of 2doms PXmatrix.
All credits for that part to him. https://github.com/2dom/PxMatrix

The Matrix of 2Dom supports a lot of display sizes, this api only
supports 64x64 displays, with a 32 scanpattern.
For hardware setup checkout the 2doms github page.

Network Protokoll:
---------------------------------------------------------------------
The network protokoll is designed to transfer images to the matrix
using a network connection. One packet should specifiy one row of
pixels. Only the command byte is mandatory, evry other byte depends
on the specified command. The length of a command is specifyed in the
code so diffrent versions of the protocoll won't be compatible.


It hase the following basic parts:
*  Command section (1 Byte)
*  Pixel colors for RGB values (64*3 = 192 Byte)(Optional)

Its based on a TCP Stream so it is verry important to keep the correct length.
If the server recives an unspecifyed command pattern, it should close
the TCP connection. So the client can handle the error.

The command byte is containing the command pattern. Currently supported
command pattern are:

| Pattern | Description | Expected length inclusive command byte
| ------ | ------ | ------ |
| 1xxx xxxx | Use the next 192 Byte to write the row xxxxxxx | 193 |
| 0000 0000 | Clear the complete screen | 1 |
| 0000 0001 | Close the connection| 1 |
| 0xxx xxxx | Not specifyed | -- |

**Write (1xxx xxxx)**:

Writes the line form left to right, 3 bytes are one pixel. A pixel looks like
RRRR RRRR, GGGG GGGG, BBBB BBBB. The pixels are not seperated by any
bit pattern from each other. Every color byte should be readable as decimal
uint_8 number, wich repesents the color value.

**Clear (0000 0000)**:

Clear the complete display, this dose not ned any other bytes.

**Close (0000 0001)**:

The client connected to the display, wants the server to close the TCP
connection instead of closing it by its owne. It shouldn't matter if you
use this command to close a connection or if you just stop the tcp connection.

**Unspecifyed**:

Unspecifyed commands should close the TCP connection, the client can reconect.
This is the "Errorhandling" of this protocol. So your client maybe want to start
resending the last 2 lines after a connection wich was closed by the server.

Protokoll Implementation:
---------------------------------------------------------------------
** Client: **

** Server: **

   